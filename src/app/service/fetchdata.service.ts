import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { filterdata} from '../model/filter.model'
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchdataService {
  products: filterdata[];
  
  constructor(private httpClient: HttpClient) { }

  fetchAllData(){
    return this.httpClient.get(environment.BASE_URL + "?limit=100")
  }
  fetchLaunchData(launch_success, land_success, launch_year){
    let filterQuery = ""

    if (land_success != undefined)
      filterQuery = filterQuery + "&land_success=" + land_success 

    if (launch_success != undefined)
      filterQuery = filterQuery + "&launch_success=" + launch_success 

    if (launch_year != undefined)
      filterQuery = filterQuery + "&launch_year=" + launch_year 
      

    return this.httpClient.get(environment.BASE_URL + "?limit=100" + filterQuery);
  }
}
