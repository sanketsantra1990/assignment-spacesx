import { Component, OnInit } from '@angular/core';
import { FetchdataService } from '../service/fetchdata.service';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { filterdata} from '../model/filter.model'
import { Observable } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  spesX: any;
  spsxfilter:FormGroup;
  launch_year: number;
  products: any;
  launchSuccess: any;
  land_success: any;
  constructor(private FetchData: FetchdataService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.FetchData.fetchAllData().subscribe(
      res =>{
        this.spesX = res
      },
      err =>{
        console.log(err)
      });

      this.route.queryParams.subscribe(queryParams => {
        this.launchSuccess = queryParams['launchSuccess'];
        this.land_success = queryParams['land_success'];
        this.launch_year = queryParams['launch_year'];
        this.filterdata(this.launchSuccess,this.land_success, this.launch_year)
      });
  }
  filterdata(launchsuccess,land_success,launch_year){
    this.FetchData.fetchLaunchData(launchsuccess,land_success, launch_year).subscribe(
      res =>{
        this.spesX = res
      },
      err =>{
      alert(err)
      }
      
      ); 
       
  }

 

}
